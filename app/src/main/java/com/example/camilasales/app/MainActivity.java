package com.example.camilasales.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.camilasales.app.model.PeopleVO;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edTxt = (EditText) findViewById(R.id.edtPassword);
        edTxt.setOnEditorActionListener(editorListener);

        List<PeopleVO> peopleList = new ArrayList<PeopleVO>();
        for(int i = 0; i < 20; i++){
            PeopleVO vo = new PeopleVO();
            vo.name = "Nome " + i;
            vo.telefone = "Telefone " + i;

            peopleList.add(vo);
        }

        ListView listContacts = findViewById(R.id.lv_contacts);

        listContacts.setAdapter();

    }

    public TextView.OnEditorActionListener editorListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    login(textView.getRootView());
                }
            }

            return false;
        }
    };

    public void login(View view) {
        Intent intent = new Intent(this, home.class);

        startActivity(intent);
    }


}
